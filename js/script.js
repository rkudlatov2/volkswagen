_MODALS = {}

jQuery(function ($) {
    $(document).ready(function () {
        setHeader()
        setMenu()
        setTabs()
        setInputs()
    })
})


function setHeader(){
    let header = $('.header')
    $(document).scroll(()=>{
        let scrollPos = $(this).scrollTop()

        if(scrollPos>40){
            header.addClass('header--scrolled')
        }else{
            header.removeClass('header--scrolled')
        }
    })
}
function setMenu(){
    let menu = $('.header__nav')
    let toggle = $('.header__nav-toggle')

    toggle.click(()=>{
        menu.toggleClass('header__nav--active')
    })
}
function setTabs(){
    let tabsContents = $('.tabs-content')
    let tabs = $('.tab')

    tabsContents.each((idx,el)=>{
        let tabsContentItems = tabsContents.find('.tabs-content__item')

        tabsContentItems.hide()
        tabsContentItems.eq(0)
            .show()
    })

    tabsContents.each((idx,el)=>{
        let tabsContentItemsVisible = tabsContents.find('.tabs-content__item:visible')
        let id = tabsContentItemsVisible.attr('id')

        tabs.filter('[href="#'+id+'"]').addClass('tab--active')
    })

    $(document).click(function(e){
        if(!$(e.target).is(tabs)) return

        e.preventDefault()

        let trg = $(e.target)
        let id = trg.attr('href')

        trg.siblings().removeClass('tab--active')
        trg.addClass('tab--active')

        $(id).siblings().hide()
        $(id).fadeIn(100)
    })
}
function setInputs(){
    let inputs = $('input')
    
    inputs.on('input change',function(){
        let trg = $(this)
        let val = trg.val()

        if(val){
            trg.addClass('--is-not-empty')
        }else{
            trg.removeClass('--is-not-empty')
        }
    })
}